package com.tiantian.shop.cache;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.tiantian.shop.model.Goods;
import com.tiantian.shop.model.Props;
import com.tiantian.shop.model.ShopProps;
import com.tiantian.shop.model.Wines;
import com.tiantian.shop.utils.CacheUtils;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class LocalCache {
    // 10分钟的有效时间
    private static final Cache<String, Optional<Wines>> winesCache = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .build();

    // 10分钟的有效时间
    private static final Cache<String, Optional<Props>> propsCache = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .build();

    // 10分钟的有效时间
    private static final Cache<String, Optional<List<ShopProps>>> shopPropsCache = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .build();

    // 10分钟的有效时间
    private static final Cache<String, Optional<Goods>> goodsCache = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .build();

    private static final Map<String, ShopProps> shopPropsMap = new ConcurrentHashMap<>();

    public static Wines getWines(final String code) {
        if (code == null) {
            return null;
        }
        try {
            Optional<Wines> optional = winesCache.get(code, new Callable<Optional<Wines>>() {
                @Override
                public Optional<Wines> call() throws Exception {
                    List<Wines> winesList = CacheUtils.getAllWines();
                    if (winesList != null && winesList.size() > 0) {
                        for (Wines wines : winesList) {
                            if (wines.getCode().equalsIgnoreCase(code)) {
                                return Optional.fromNullable(wines);
                            }
                        }
                    }
                    return Optional.absent();
                }
            });
            if (optional.isPresent()) {
                return optional.get();
            } else {
                winesCache.invalidate(code);
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Props getProps(final String code) {
        if (code == null) {
            return null;
        }
        try {
            Optional<Props> optional = propsCache.get(code, new Callable<Optional<Props>>() {
                @Override
                public Optional<Props> call() throws Exception {
                    List<Props> propsList = CacheUtils.getAllProps();
                    System.out.println(JSON.toJSONString(propsList));
                    if (propsList != null && propsList.size() > 0) {
                        for (Props props : propsList) {
                            if (props.getCode().equalsIgnoreCase(code)) {
                                return Optional.fromNullable(props);
                            }
                        }
                    }
                    return Optional.absent();
                }
            });
            if (optional.isPresent()) {
                return optional.get();
            } else {
                propsCache.invalidate(code);
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Goods getGoods(final String code) {
        if (code == null) {
            return null;
        }
        try {
            Optional<Goods> optional = goodsCache.get(code, new Callable<Optional<Goods>>() {
                @Override
                public Optional<Goods> call() throws Exception {
                    List<Goods> goodsList = CacheUtils.getAllGoods();
                    if (goodsList != null && goodsList.size() > 0) {
                        for (Goods goods : goodsList) {
                            if (goods.getCode().equalsIgnoreCase(code)) {
                                return Optional.fromNullable(goods);
                            }
                        }
                    }
                    return Optional.absent();
                }
            });
            if (optional.isPresent()) {
                return optional.get();
            } else {
                goodsCache.invalidate(code);
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<ShopProps> getShopProps() {
        try {
            Optional<List<ShopProps>> optional = shopPropsCache.get("all_shops", new Callable<Optional<List<ShopProps>>>() {
                @Override
                public Optional<List<ShopProps>> call() throws Exception {
                    List<ShopProps> propsList = CacheUtils.getAllShopProps();
                    if (propsList != null && propsList.size() > 0) {
                        shopPropsMap.clear();
                        for (ShopProps shopProps : propsList) {
                             shopPropsMap.put(shopProps.getCode(), shopProps);
                        }
                        return Optional.fromNullable(propsList);
                    }
                    return Optional.absent();
                }
            });
            if (optional.isPresent()) {
                return optional.get();
            } else {
                shopPropsCache.invalidate("all_shops");
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ShopProps getShopPropsByCode(String code) {
       // 先刷一下缓存
       getShopProps();
       return shopPropsMap.get(code);
    }
}
