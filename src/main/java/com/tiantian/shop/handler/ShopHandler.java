package com.tiantian.shop.handler;

import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.core.thrift.account.UserResponse;
import com.tiantian.shop.cache.LocalCache;
import com.tiantian.shop.entity.OrderAddressEntity;
import com.tiantian.shop.entity.OrderEntity;
import com.tiantian.shop.manager.AddressManager;
import com.tiantian.shop.manager.OrderAddressManager;
import com.tiantian.shop.manager.OrderManager;
import com.tiantian.shop.manager.ShopPropManager;
import com.tiantian.shop.model.Goods;
import com.tiantian.shop.model.Props;
import com.tiantian.shop.model.ShopProps;
import com.tiantian.shop.model.Wines;
import com.tiantian.shop.thrift.*;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ShopHandler implements ShopService.Iface {
    static Logger LOG = LoggerFactory.getLogger(ShopHandler.class);
    @Override
    public String getServiceVersion() throws TException {
       return shopConstants.version;
    }

    /**
     * 玩家购买美酒
     * @param code
     */
    public WinesResponse buyWines(String userId, String code) {
       WinesResponse winesResponse = new WinesResponse();
        UserDetail userDetail = null;
        try {
            userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        if (userDetail == null) {
           winesResponse.setStatus(-1);
           winesResponse.setMessage("用户信息不存在");
           return winesResponse;
       }
       Wines wines = LocalCache.getWines(code);
       if (wines == null) {
           winesResponse.setStatus(-2);
           winesResponse.setMessage("美酒信息不存在");
           return winesResponse;
       }
       long costDiamond = wines.getCostDiamond();
       if (userDetail.getDiamond() < costDiamond) {
           winesResponse.setStatus(-3);
           winesResponse.setMessage("钻石不足");
           return winesResponse;
       }
       long addMoney = wines.getGiftMoney();
       //  扣除钻石, 增加金币
        boolean result = false;
        try {
            result = AccountIface.instance().iface().addUserMoneyAndReduceDiamond(userId, addMoney, costDiamond);
        } catch (TException e) {
            e.printStackTrace();
        }
        if(!result) { //钻石数量不足
           winesResponse.setStatus(-3);
           winesResponse.setMessage("钻石不够");
           return winesResponse;
       }
        winesResponse.setStatus(0);
        winesResponse.setMessage("购买成功");
        winesResponse.setGiftMoney(addMoney);
        return winesResponse;
    }

    /**
     * 玩家购买道具
     * @param code
     */
    public PropsResponse buyProps(String userId, String code) {
        UserDetail userDetail = null;
        try {
            userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        PropsResponse propsResponse = new PropsResponse();
        if (userDetail == null) {
            propsResponse.setStatus(-1);
            propsResponse.setMessage("用户信息不存在");
            return propsResponse;
        }
        Props props = LocalCache.getProps(code);
        if (props == null) {
            propsResponse.setStatus(-2);
            propsResponse.setMessage("道具信息不存在");
            return propsResponse;
        }
        String goodsCode = props.getGoodsCode();
        Goods goods = LocalCache.getGoods(goodsCode);
        if (goods == null) {
            propsResponse.setStatus(-2);
            propsResponse.setMessage("道具信息不存在");
            return propsResponse;
        }


        long costMoney = props.getCostMoney();
        if (costMoney > 0) {
            if (userDetail.getMoney() < costMoney) {
                propsResponse.setStatus(-3);
                propsResponse.setMessage("金币不足");
                return propsResponse;
            } else {
                boolean result = false;
                try {
                    result = AccountIface.instance().iface().reduceUserMoney(userId, costMoney);
                } catch (TException e) {
                    e.printStackTrace();
                }
                if (!result) {
                    propsResponse.setStatus(-3);
                    propsResponse.setMessage("金币不足");
                    return propsResponse;
                }
            }
        }
        long costDiamond = props.getCostDiamond();
        if (costDiamond > 0) {
            if (userDetail.getDiamond() < costDiamond) {
                propsResponse.setStatus(-4);
                propsResponse.setMessage("钻石不足");
                return propsResponse;
            } else {
                boolean result = false;
                try {
                    result = AccountIface.instance().iface().reduceUserDiamond(userId, costDiamond);
                } catch (TException e) {
                    e.printStackTrace();
                }
                if (!result) {
                    propsResponse.setStatus(-4);
                    propsResponse.setMessage("钻石不足");
                    return propsResponse;
                }
            }
        }
        //添加道具和物品
        long number = props.getNumbers();
        try {
            AccountIface.instance().iface().saveOrUpdateUserProps(userId, goodsCode, goods.getName(), goods.getImgUrl(),
                    goods.getDesc(), number);
        } catch (TException e) {
            e.printStackTrace();
        }
        propsResponse.setStatus(0);
        propsResponse.setMessage("购买成功");
        return propsResponse;
    }

    public List<SpProps> shopList(String userId, long beginIndex, long count) {
       List<ShopProps> allShops = LocalCache.getShopProps();
       List<SpProps> spPropsList = new ArrayList<>();
       if (allShops != null && allShops.size() > 0) {
           long begin = Math.min(beginIndex, allShops.size() - 1);
           long end = Math.min(begin + count + 1, allShops.size());
           List<ShopProps> shopPropsList = allShops.subList((int) begin, (int) end);
           if (shopPropsList.size() == 0) {
               return new ArrayList<>();
           }
           List<String> codes = new ArrayList<>();
           for (ShopProps shopProps : shopPropsList) {
               codes.add(shopProps.getCode());
           }
           // 查询剩余数量
           Map<String, Long> amountMap = ShopPropManager.getShoPropsLeftAmount(codes);
           for (ShopProps shopProps : shopPropsList) {
                SpProps spProps = new SpProps(shopProps.getCode(), shopProps.getName(), shopProps.getImgUrl(),
                        shopProps.getCostMoney(), shopProps.getCostShell(), shopProps.getType(), shopProps.getMark(),
                        shopProps.getConditionDesc(), amountMap.get(shopProps.getCode()));
               spPropsList.add(spProps);
           }
       }
       return spPropsList;
    }

    // 兑换道具
    public ShopResponse exchange(String code, String userId) {
        ShopProps shopProps = LocalCache.getShopPropsByCode(code);
        if (shopProps == null) {
            return new ShopResponse(101, "道具已下架");
        }
        UserDetail userDetail = null;
        try {
            userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        if (userDetail == null) {
            return new ShopResponse(102, "用户信息不存在");
        }
        if (shopProps.getCostShell() > userDetail.getShellMoney()) {
            return new ShopResponse(103, "用户贝壳数量不足");
        }
        if (shopProps.getCostMoney() > userDetail.getMoney()) {
            return new ShopResponse(104, "用户金币数量不足");
        }
        String orderType = "hf"; // 默认话费
        String addressId = "";
        Address address = AddressManager.getUserAddress(userId);
        // 是否需要完成玩家地址信息
        if("physical".equalsIgnoreCase(shopProps.getType())) {
            orderType = "physical";
            if (address == null) {
                return new ShopResponse(105, "实物道具需要填写发货地址");
            }
            addressId = address.getId();
        }
        //TODO 判断各种限制条件 比如剩余数量, 每个人每天兑换次数
        long leftAmount = ShopPropManager.getLeftAmount(shopProps.getCode());
        if (leftAmount != -1) { // 大于等于0表示不限制数量
            if(leftAmount == 0) {
              return new ShopResponse(108, "道具剩余数量不足");
            }
            boolean reduceRet = ShopPropManager.reduceShoPropsLeftAmount(shopProps.getCode(), 1);
            if (!reduceRet) {
                return new ShopResponse(108, "道具剩余数量不足");
            }
        }
        boolean ret = false;
        try {
            ret = AccountIface.instance().iface().reduceUserShellAndMoney(userId, shopProps.getCostShell(), shopProps.getCostMoney());
        } catch (TException e) {
            e.printStackTrace();
        }
        if (!ret) {
            return new ShopResponse(106, "用户贝壳或金币数量不足");
        }
        if (address != null) {
            OrderAddressEntity orderAddressEntity = OrderAddressManager.addOrderAddress(address.getReceiptAddress(), address.getEmail(),
                    address.getZipCode(), address.getReceiverName(), address.getMobileNumber(),
                    address.getTelephoneNumber());
            if (orderAddressEntity != null) {
                addressId = orderAddressEntity.getId();
            }
        }
        // 保存订单
        OrderEntity order = OrderManager.addOrder(userId, shopProps.getCode(), shopProps.getName(), addressId, orderType);
        if(order != null) {
           if (address != null) {

           }
           return new ShopResponse(0, "兑换成功");
        }
        return new ShopResponse(107, "订单保存失败");
    }

    public List<Order> getOrderPage(String userId, long beginIndex, long count) {
        List<OrderEntity> orderEntityList = OrderManager.getOrderListPage(userId, beginIndex, count);
        List<Order> orders = new ArrayList<>();
        for (OrderEntity orderEntity : orderEntityList) {
             Order order = new Order(orderEntity.getOrderId(), orderEntity.getUserId() ,orderEntity.getShopPropCode(),
                     orderEntity.getShopPropName(), orderEntity.getStatus(), orderEntity.getCreateDate());
             orders.add(order);
        }
        return orders;
    }

    public Address getUserAddress(String userId) {
        return AddressManager.getUserAddress(userId);
    }

    public Address addUserAddress(String userId,  String receiptAddress, String email, String zipCode, String receiverName,
                                  String mobileNumber, String telephoneNumber) {
        Address address = getUserAddress(userId);
        if (address != null && StringUtils.isNotBlank(address.getId())) {
            updateUserAddress(address.getId(), userId, receiptAddress, email, zipCode, receiverName, mobileNumber, telephoneNumber);
            address.setReceiptAddress(receiptAddress);
            address.setEmail(email);
            address.setReceiverName(receiverName);
            address.setMobileNumber(mobileNumber);
            address.setTelephoneNumber(telephoneNumber);
            return address;
        }
        return AddressManager.addAddress(userId, receiptAddress, email, zipCode, receiverName,
                  mobileNumber, telephoneNumber);
    }

    public boolean updateUserAddress(String addressId, String userId, String receiptAddress, String email,
                                     String zipCode, String receiverName,
                                     String mobileNumber, String telephoneNumber) {
        return AddressManager.updateUserAddress(addressId, userId, receiptAddress, email, receiverName,
                  mobileNumber);
    }

    public ShopResponse exchangeJdEcard(String userId, String code) throws TException {
        Address address = AddressManager.getUserAddress(userId);
        if (address == null || StringUtils.isBlank(address.getEmail())) {
            return new ShopResponse(4, "请完善收货地址中的邮箱信息");
        }
        UserResponse userResponse = AccountIface.instance().iface().exchangeJdEcard(userId, code);
        if (userResponse != null) {
            if (userResponse.getStatus() == 1) {
                OrderAddressEntity orderAddressEntity = OrderAddressManager.addOrderAddress(address.getReceiptAddress(), address.getEmail(),
                        address.getZipCode(), address.getReceiverName(), address.getMobileNumber(),
                        address.getTelephoneNumber());

                //生成兑换订单
               String orderId = generateEcardOrder(userId, code, orderAddressEntity.getId());

               if (StringUtils.isNotBlank(orderId)) {
                   return new ShopResponse(0, "兑换成功");
               }
               LOG.error("exchangeJdEcard FAIL: userId :" + userId + ", code:" + code);
            }
            else {
                LOG.error("exchangeJdEcard FAIL: userId :" + userId + ", code:" + code +", reason:" + userResponse.getMessage());
                return new ShopResponse(userResponse.getStatus(), userResponse.getMessage());
            }
        }
        return new ShopResponse(-1, "兑换失败");
    }

    private String generateEcardOrder(String userId, String code, String addressId) {
        Goods goods = LocalCache.getGoods(code);
        if (goods != null) {
            String orderType = "jd_ecard";
            // 保存订单
            OrderEntity order = OrderManager.addOrder(userId, code, goods.getName(), addressId, orderType);
            if(order != null) {
               return order.getOrderId();
            }
            LOG.error("jd_ecard not exists: userId :" + userId + ", code:" + code);
        } else {
            LOG.error("jd_ecard not exists: userId :" + userId + ", code:" + code);
        }

        return null;
    }

}
