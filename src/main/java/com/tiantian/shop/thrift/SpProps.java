/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.tiantian.shop.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (0.9.3)", date = "2016-05-12")
public class SpProps implements org.apache.thrift.TBase<SpProps, SpProps._Fields>, java.io.Serializable, Cloneable, Comparable<SpProps> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("SpProps");

  private static final org.apache.thrift.protocol.TField CODE_FIELD_DESC = new org.apache.thrift.protocol.TField("code", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField NAME_FIELD_DESC = new org.apache.thrift.protocol.TField("name", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField IMG_URL_FIELD_DESC = new org.apache.thrift.protocol.TField("imgUrl", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField COST_MONEY_FIELD_DESC = new org.apache.thrift.protocol.TField("costMoney", org.apache.thrift.protocol.TType.I64, (short)4);
  private static final org.apache.thrift.protocol.TField COST_SHELL_FIELD_DESC = new org.apache.thrift.protocol.TField("costShell", org.apache.thrift.protocol.TType.I64, (short)5);
  private static final org.apache.thrift.protocol.TField TYPE_FIELD_DESC = new org.apache.thrift.protocol.TField("type", org.apache.thrift.protocol.TType.STRING, (short)6);
  private static final org.apache.thrift.protocol.TField MARK_FIELD_DESC = new org.apache.thrift.protocol.TField("mark", org.apache.thrift.protocol.TType.STRING, (short)7);
  private static final org.apache.thrift.protocol.TField CONDITION_DESC_FIELD_DESC = new org.apache.thrift.protocol.TField("conditionDesc", org.apache.thrift.protocol.TType.STRING, (short)8);
  private static final org.apache.thrift.protocol.TField LEFT_AMOUNT_FIELD_DESC = new org.apache.thrift.protocol.TField("leftAmount", org.apache.thrift.protocol.TType.I64, (short)9);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new SpPropsStandardSchemeFactory());
    schemes.put(TupleScheme.class, new SpPropsTupleSchemeFactory());
  }

  public String code; // required
  public String name; // required
  public String imgUrl; // required
  public long costMoney; // required
  public long costShell; // required
  public String type; // required
  public String mark; // required
  public String conditionDesc; // required
  public long leftAmount; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    CODE((short)1, "code"),
    NAME((short)2, "name"),
    IMG_URL((short)3, "imgUrl"),
    COST_MONEY((short)4, "costMoney"),
    COST_SHELL((short)5, "costShell"),
    TYPE((short)6, "type"),
    MARK((short)7, "mark"),
    CONDITION_DESC((short)8, "conditionDesc"),
    LEFT_AMOUNT((short)9, "leftAmount");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // CODE
          return CODE;
        case 2: // NAME
          return NAME;
        case 3: // IMG_URL
          return IMG_URL;
        case 4: // COST_MONEY
          return COST_MONEY;
        case 5: // COST_SHELL
          return COST_SHELL;
        case 6: // TYPE
          return TYPE;
        case 7: // MARK
          return MARK;
        case 8: // CONDITION_DESC
          return CONDITION_DESC;
        case 9: // LEFT_AMOUNT
          return LEFT_AMOUNT;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __COSTMONEY_ISSET_ID = 0;
  private static final int __COSTSHELL_ISSET_ID = 1;
  private static final int __LEFTAMOUNT_ISSET_ID = 2;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.CODE, new org.apache.thrift.meta_data.FieldMetaData("code", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.NAME, new org.apache.thrift.meta_data.FieldMetaData("name", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.IMG_URL, new org.apache.thrift.meta_data.FieldMetaData("imgUrl", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.COST_MONEY, new org.apache.thrift.meta_data.FieldMetaData("costMoney", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.COST_SHELL, new org.apache.thrift.meta_data.FieldMetaData("costShell", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.TYPE, new org.apache.thrift.meta_data.FieldMetaData("type", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.MARK, new org.apache.thrift.meta_data.FieldMetaData("mark", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.CONDITION_DESC, new org.apache.thrift.meta_data.FieldMetaData("conditionDesc", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.LEFT_AMOUNT, new org.apache.thrift.meta_data.FieldMetaData("leftAmount", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(SpProps.class, metaDataMap);
  }

  public SpProps() {
  }

  public SpProps(
    String code,
    String name,
    String imgUrl,
    long costMoney,
    long costShell,
    String type,
    String mark,
    String conditionDesc,
    long leftAmount)
  {
    this();
    this.code = code;
    this.name = name;
    this.imgUrl = imgUrl;
    this.costMoney = costMoney;
    setCostMoneyIsSet(true);
    this.costShell = costShell;
    setCostShellIsSet(true);
    this.type = type;
    this.mark = mark;
    this.conditionDesc = conditionDesc;
    this.leftAmount = leftAmount;
    setLeftAmountIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public SpProps(SpProps other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetCode()) {
      this.code = other.code;
    }
    if (other.isSetName()) {
      this.name = other.name;
    }
    if (other.isSetImgUrl()) {
      this.imgUrl = other.imgUrl;
    }
    this.costMoney = other.costMoney;
    this.costShell = other.costShell;
    if (other.isSetType()) {
      this.type = other.type;
    }
    if (other.isSetMark()) {
      this.mark = other.mark;
    }
    if (other.isSetConditionDesc()) {
      this.conditionDesc = other.conditionDesc;
    }
    this.leftAmount = other.leftAmount;
  }

  public SpProps deepCopy() {
    return new SpProps(this);
  }

  @Override
  public void clear() {
    this.code = null;
    this.name = null;
    this.imgUrl = null;
    setCostMoneyIsSet(false);
    this.costMoney = 0;
    setCostShellIsSet(false);
    this.costShell = 0;
    this.type = null;
    this.mark = null;
    this.conditionDesc = null;
    setLeftAmountIsSet(false);
    this.leftAmount = 0;
  }

  public String getCode() {
    return this.code;
  }

  public SpProps setCode(String code) {
    this.code = code;
    return this;
  }

  public void unsetCode() {
    this.code = null;
  }

  /** Returns true if field code is set (has been assigned a value) and false otherwise */
  public boolean isSetCode() {
    return this.code != null;
  }

  public void setCodeIsSet(boolean value) {
    if (!value) {
      this.code = null;
    }
  }

  public String getName() {
    return this.name;
  }

  public SpProps setName(String name) {
    this.name = name;
    return this;
  }

  public void unsetName() {
    this.name = null;
  }

  /** Returns true if field name is set (has been assigned a value) and false otherwise */
  public boolean isSetName() {
    return this.name != null;
  }

  public void setNameIsSet(boolean value) {
    if (!value) {
      this.name = null;
    }
  }

  public String getImgUrl() {
    return this.imgUrl;
  }

  public SpProps setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
    return this;
  }

  public void unsetImgUrl() {
    this.imgUrl = null;
  }

  /** Returns true if field imgUrl is set (has been assigned a value) and false otherwise */
  public boolean isSetImgUrl() {
    return this.imgUrl != null;
  }

  public void setImgUrlIsSet(boolean value) {
    if (!value) {
      this.imgUrl = null;
    }
  }

  public long getCostMoney() {
    return this.costMoney;
  }

  public SpProps setCostMoney(long costMoney) {
    this.costMoney = costMoney;
    setCostMoneyIsSet(true);
    return this;
  }

  public void unsetCostMoney() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __COSTMONEY_ISSET_ID);
  }

  /** Returns true if field costMoney is set (has been assigned a value) and false otherwise */
  public boolean isSetCostMoney() {
    return EncodingUtils.testBit(__isset_bitfield, __COSTMONEY_ISSET_ID);
  }

  public void setCostMoneyIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __COSTMONEY_ISSET_ID, value);
  }

  public long getCostShell() {
    return this.costShell;
  }

  public SpProps setCostShell(long costShell) {
    this.costShell = costShell;
    setCostShellIsSet(true);
    return this;
  }

  public void unsetCostShell() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __COSTSHELL_ISSET_ID);
  }

  /** Returns true if field costShell is set (has been assigned a value) and false otherwise */
  public boolean isSetCostShell() {
    return EncodingUtils.testBit(__isset_bitfield, __COSTSHELL_ISSET_ID);
  }

  public void setCostShellIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __COSTSHELL_ISSET_ID, value);
  }

  public String getType() {
    return this.type;
  }

  public SpProps setType(String type) {
    this.type = type;
    return this;
  }

  public void unsetType() {
    this.type = null;
  }

  /** Returns true if field type is set (has been assigned a value) and false otherwise */
  public boolean isSetType() {
    return this.type != null;
  }

  public void setTypeIsSet(boolean value) {
    if (!value) {
      this.type = null;
    }
  }

  public String getMark() {
    return this.mark;
  }

  public SpProps setMark(String mark) {
    this.mark = mark;
    return this;
  }

  public void unsetMark() {
    this.mark = null;
  }

  /** Returns true if field mark is set (has been assigned a value) and false otherwise */
  public boolean isSetMark() {
    return this.mark != null;
  }

  public void setMarkIsSet(boolean value) {
    if (!value) {
      this.mark = null;
    }
  }

  public String getConditionDesc() {
    return this.conditionDesc;
  }

  public SpProps setConditionDesc(String conditionDesc) {
    this.conditionDesc = conditionDesc;
    return this;
  }

  public void unsetConditionDesc() {
    this.conditionDesc = null;
  }

  /** Returns true if field conditionDesc is set (has been assigned a value) and false otherwise */
  public boolean isSetConditionDesc() {
    return this.conditionDesc != null;
  }

  public void setConditionDescIsSet(boolean value) {
    if (!value) {
      this.conditionDesc = null;
    }
  }

  public long getLeftAmount() {
    return this.leftAmount;
  }

  public SpProps setLeftAmount(long leftAmount) {
    this.leftAmount = leftAmount;
    setLeftAmountIsSet(true);
    return this;
  }

  public void unsetLeftAmount() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __LEFTAMOUNT_ISSET_ID);
  }

  /** Returns true if field leftAmount is set (has been assigned a value) and false otherwise */
  public boolean isSetLeftAmount() {
    return EncodingUtils.testBit(__isset_bitfield, __LEFTAMOUNT_ISSET_ID);
  }

  public void setLeftAmountIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __LEFTAMOUNT_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case CODE:
      if (value == null) {
        unsetCode();
      } else {
        setCode((String)value);
      }
      break;

    case NAME:
      if (value == null) {
        unsetName();
      } else {
        setName((String)value);
      }
      break;

    case IMG_URL:
      if (value == null) {
        unsetImgUrl();
      } else {
        setImgUrl((String)value);
      }
      break;

    case COST_MONEY:
      if (value == null) {
        unsetCostMoney();
      } else {
        setCostMoney((Long)value);
      }
      break;

    case COST_SHELL:
      if (value == null) {
        unsetCostShell();
      } else {
        setCostShell((Long)value);
      }
      break;

    case TYPE:
      if (value == null) {
        unsetType();
      } else {
        setType((String)value);
      }
      break;

    case MARK:
      if (value == null) {
        unsetMark();
      } else {
        setMark((String)value);
      }
      break;

    case CONDITION_DESC:
      if (value == null) {
        unsetConditionDesc();
      } else {
        setConditionDesc((String)value);
      }
      break;

    case LEFT_AMOUNT:
      if (value == null) {
        unsetLeftAmount();
      } else {
        setLeftAmount((Long)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case CODE:
      return getCode();

    case NAME:
      return getName();

    case IMG_URL:
      return getImgUrl();

    case COST_MONEY:
      return getCostMoney();

    case COST_SHELL:
      return getCostShell();

    case TYPE:
      return getType();

    case MARK:
      return getMark();

    case CONDITION_DESC:
      return getConditionDesc();

    case LEFT_AMOUNT:
      return getLeftAmount();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case CODE:
      return isSetCode();
    case NAME:
      return isSetName();
    case IMG_URL:
      return isSetImgUrl();
    case COST_MONEY:
      return isSetCostMoney();
    case COST_SHELL:
      return isSetCostShell();
    case TYPE:
      return isSetType();
    case MARK:
      return isSetMark();
    case CONDITION_DESC:
      return isSetConditionDesc();
    case LEFT_AMOUNT:
      return isSetLeftAmount();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof SpProps)
      return this.equals((SpProps)that);
    return false;
  }

  public boolean equals(SpProps that) {
    if (that == null)
      return false;

    boolean this_present_code = true && this.isSetCode();
    boolean that_present_code = true && that.isSetCode();
    if (this_present_code || that_present_code) {
      if (!(this_present_code && that_present_code))
        return false;
      if (!this.code.equals(that.code))
        return false;
    }

    boolean this_present_name = true && this.isSetName();
    boolean that_present_name = true && that.isSetName();
    if (this_present_name || that_present_name) {
      if (!(this_present_name && that_present_name))
        return false;
      if (!this.name.equals(that.name))
        return false;
    }

    boolean this_present_imgUrl = true && this.isSetImgUrl();
    boolean that_present_imgUrl = true && that.isSetImgUrl();
    if (this_present_imgUrl || that_present_imgUrl) {
      if (!(this_present_imgUrl && that_present_imgUrl))
        return false;
      if (!this.imgUrl.equals(that.imgUrl))
        return false;
    }

    boolean this_present_costMoney = true;
    boolean that_present_costMoney = true;
    if (this_present_costMoney || that_present_costMoney) {
      if (!(this_present_costMoney && that_present_costMoney))
        return false;
      if (this.costMoney != that.costMoney)
        return false;
    }

    boolean this_present_costShell = true;
    boolean that_present_costShell = true;
    if (this_present_costShell || that_present_costShell) {
      if (!(this_present_costShell && that_present_costShell))
        return false;
      if (this.costShell != that.costShell)
        return false;
    }

    boolean this_present_type = true && this.isSetType();
    boolean that_present_type = true && that.isSetType();
    if (this_present_type || that_present_type) {
      if (!(this_present_type && that_present_type))
        return false;
      if (!this.type.equals(that.type))
        return false;
    }

    boolean this_present_mark = true && this.isSetMark();
    boolean that_present_mark = true && that.isSetMark();
    if (this_present_mark || that_present_mark) {
      if (!(this_present_mark && that_present_mark))
        return false;
      if (!this.mark.equals(that.mark))
        return false;
    }

    boolean this_present_conditionDesc = true && this.isSetConditionDesc();
    boolean that_present_conditionDesc = true && that.isSetConditionDesc();
    if (this_present_conditionDesc || that_present_conditionDesc) {
      if (!(this_present_conditionDesc && that_present_conditionDesc))
        return false;
      if (!this.conditionDesc.equals(that.conditionDesc))
        return false;
    }

    boolean this_present_leftAmount = true;
    boolean that_present_leftAmount = true;
    if (this_present_leftAmount || that_present_leftAmount) {
      if (!(this_present_leftAmount && that_present_leftAmount))
        return false;
      if (this.leftAmount != that.leftAmount)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_code = true && (isSetCode());
    list.add(present_code);
    if (present_code)
      list.add(code);

    boolean present_name = true && (isSetName());
    list.add(present_name);
    if (present_name)
      list.add(name);

    boolean present_imgUrl = true && (isSetImgUrl());
    list.add(present_imgUrl);
    if (present_imgUrl)
      list.add(imgUrl);

    boolean present_costMoney = true;
    list.add(present_costMoney);
    if (present_costMoney)
      list.add(costMoney);

    boolean present_costShell = true;
    list.add(present_costShell);
    if (present_costShell)
      list.add(costShell);

    boolean present_type = true && (isSetType());
    list.add(present_type);
    if (present_type)
      list.add(type);

    boolean present_mark = true && (isSetMark());
    list.add(present_mark);
    if (present_mark)
      list.add(mark);

    boolean present_conditionDesc = true && (isSetConditionDesc());
    list.add(present_conditionDesc);
    if (present_conditionDesc)
      list.add(conditionDesc);

    boolean present_leftAmount = true;
    list.add(present_leftAmount);
    if (present_leftAmount)
      list.add(leftAmount);

    return list.hashCode();
  }

  @Override
  public int compareTo(SpProps other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetCode()).compareTo(other.isSetCode());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCode()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.code, other.code);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetName()).compareTo(other.isSetName());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetName()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.name, other.name);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetImgUrl()).compareTo(other.isSetImgUrl());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetImgUrl()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.imgUrl, other.imgUrl);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetCostMoney()).compareTo(other.isSetCostMoney());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCostMoney()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.costMoney, other.costMoney);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetCostShell()).compareTo(other.isSetCostShell());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCostShell()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.costShell, other.costShell);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetType()).compareTo(other.isSetType());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetType()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.type, other.type);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetMark()).compareTo(other.isSetMark());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMark()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.mark, other.mark);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetConditionDesc()).compareTo(other.isSetConditionDesc());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetConditionDesc()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.conditionDesc, other.conditionDesc);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetLeftAmount()).compareTo(other.isSetLeftAmount());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLeftAmount()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.leftAmount, other.leftAmount);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("SpProps(");
    boolean first = true;

    sb.append("code:");
    if (this.code == null) {
      sb.append("null");
    } else {
      sb.append(this.code);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("name:");
    if (this.name == null) {
      sb.append("null");
    } else {
      sb.append(this.name);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("imgUrl:");
    if (this.imgUrl == null) {
      sb.append("null");
    } else {
      sb.append(this.imgUrl);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("costMoney:");
    sb.append(this.costMoney);
    first = false;
    if (!first) sb.append(", ");
    sb.append("costShell:");
    sb.append(this.costShell);
    first = false;
    if (!first) sb.append(", ");
    sb.append("type:");
    if (this.type == null) {
      sb.append("null");
    } else {
      sb.append(this.type);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("mark:");
    if (this.mark == null) {
      sb.append("null");
    } else {
      sb.append(this.mark);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("conditionDesc:");
    if (this.conditionDesc == null) {
      sb.append("null");
    } else {
      sb.append(this.conditionDesc);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("leftAmount:");
    sb.append(this.leftAmount);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class SpPropsStandardSchemeFactory implements SchemeFactory {
    public SpPropsStandardScheme getScheme() {
      return new SpPropsStandardScheme();
    }
  }

  private static class SpPropsStandardScheme extends StandardScheme<SpProps> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, SpProps struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // CODE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.code = iprot.readString();
              struct.setCodeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // NAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.name = iprot.readString();
              struct.setNameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // IMG_URL
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.imgUrl = iprot.readString();
              struct.setImgUrlIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // COST_MONEY
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.costMoney = iprot.readI64();
              struct.setCostMoneyIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // COST_SHELL
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.costShell = iprot.readI64();
              struct.setCostShellIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 6: // TYPE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.type = iprot.readString();
              struct.setTypeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 7: // MARK
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.mark = iprot.readString();
              struct.setMarkIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 8: // CONDITION_DESC
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.conditionDesc = iprot.readString();
              struct.setConditionDescIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 9: // LEFT_AMOUNT
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.leftAmount = iprot.readI64();
              struct.setLeftAmountIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, SpProps struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.code != null) {
        oprot.writeFieldBegin(CODE_FIELD_DESC);
        oprot.writeString(struct.code);
        oprot.writeFieldEnd();
      }
      if (struct.name != null) {
        oprot.writeFieldBegin(NAME_FIELD_DESC);
        oprot.writeString(struct.name);
        oprot.writeFieldEnd();
      }
      if (struct.imgUrl != null) {
        oprot.writeFieldBegin(IMG_URL_FIELD_DESC);
        oprot.writeString(struct.imgUrl);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(COST_MONEY_FIELD_DESC);
      oprot.writeI64(struct.costMoney);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(COST_SHELL_FIELD_DESC);
      oprot.writeI64(struct.costShell);
      oprot.writeFieldEnd();
      if (struct.type != null) {
        oprot.writeFieldBegin(TYPE_FIELD_DESC);
        oprot.writeString(struct.type);
        oprot.writeFieldEnd();
      }
      if (struct.mark != null) {
        oprot.writeFieldBegin(MARK_FIELD_DESC);
        oprot.writeString(struct.mark);
        oprot.writeFieldEnd();
      }
      if (struct.conditionDesc != null) {
        oprot.writeFieldBegin(CONDITION_DESC_FIELD_DESC);
        oprot.writeString(struct.conditionDesc);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(LEFT_AMOUNT_FIELD_DESC);
      oprot.writeI64(struct.leftAmount);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class SpPropsTupleSchemeFactory implements SchemeFactory {
    public SpPropsTupleScheme getScheme() {
      return new SpPropsTupleScheme();
    }
  }

  private static class SpPropsTupleScheme extends TupleScheme<SpProps> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, SpProps struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetCode()) {
        optionals.set(0);
      }
      if (struct.isSetName()) {
        optionals.set(1);
      }
      if (struct.isSetImgUrl()) {
        optionals.set(2);
      }
      if (struct.isSetCostMoney()) {
        optionals.set(3);
      }
      if (struct.isSetCostShell()) {
        optionals.set(4);
      }
      if (struct.isSetType()) {
        optionals.set(5);
      }
      if (struct.isSetMark()) {
        optionals.set(6);
      }
      if (struct.isSetConditionDesc()) {
        optionals.set(7);
      }
      if (struct.isSetLeftAmount()) {
        optionals.set(8);
      }
      oprot.writeBitSet(optionals, 9);
      if (struct.isSetCode()) {
        oprot.writeString(struct.code);
      }
      if (struct.isSetName()) {
        oprot.writeString(struct.name);
      }
      if (struct.isSetImgUrl()) {
        oprot.writeString(struct.imgUrl);
      }
      if (struct.isSetCostMoney()) {
        oprot.writeI64(struct.costMoney);
      }
      if (struct.isSetCostShell()) {
        oprot.writeI64(struct.costShell);
      }
      if (struct.isSetType()) {
        oprot.writeString(struct.type);
      }
      if (struct.isSetMark()) {
        oprot.writeString(struct.mark);
      }
      if (struct.isSetConditionDesc()) {
        oprot.writeString(struct.conditionDesc);
      }
      if (struct.isSetLeftAmount()) {
        oprot.writeI64(struct.leftAmount);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, SpProps struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(9);
      if (incoming.get(0)) {
        struct.code = iprot.readString();
        struct.setCodeIsSet(true);
      }
      if (incoming.get(1)) {
        struct.name = iprot.readString();
        struct.setNameIsSet(true);
      }
      if (incoming.get(2)) {
        struct.imgUrl = iprot.readString();
        struct.setImgUrlIsSet(true);
      }
      if (incoming.get(3)) {
        struct.costMoney = iprot.readI64();
        struct.setCostMoneyIsSet(true);
      }
      if (incoming.get(4)) {
        struct.costShell = iprot.readI64();
        struct.setCostShellIsSet(true);
      }
      if (incoming.get(5)) {
        struct.type = iprot.readString();
        struct.setTypeIsSet(true);
      }
      if (incoming.get(6)) {
        struct.mark = iprot.readString();
        struct.setMarkIsSet(true);
      }
      if (incoming.get(7)) {
        struct.conditionDesc = iprot.readString();
        struct.setConditionDescIsSet(true);
      }
      if (incoming.get(8)) {
        struct.leftAmount = iprot.readI64();
        struct.setLeftAmountIsSet(true);
      }
    }
  }

}

