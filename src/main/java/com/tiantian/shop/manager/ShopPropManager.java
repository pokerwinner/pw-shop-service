package com.tiantian.shop.manager;

import com.tiantian.shop.data.postgresql.PGDatabase;
import com.tiantian.shop.data.postgresql.design.ShopPropTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ShopPropManager {
    static Logger LOG = LoggerFactory.getLogger(ShopPropManager.class);

    public static Map<String, Long> getShoPropsLeftAmount(List<String> codes) {
        Map<String, Long> map = new HashMap<>();
        String codeStr = "";
        for (String code : codes) {
             codeStr += "'" + code + "',";
             map.put(code, 0l);
        }
        codeStr = codeStr.substring(0, codeStr.length() - 1);
        String sql = String.format("select %s, %s from %s where %s in (%s) and %s = %d;",
                ShopPropTable.COLUMN_CODE, ShopPropTable.COLUMN_LEFT_AMOUNT,
                ShopPropTable.TABLE_SHOP_PROPS, ShopPropTable.COLUMN_CODE, codeStr,
                ShopPropTable.COLUMN_AVAILABLE , 1);
        LOG.info("getShoPropsLeftAmount ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                   String code = rs.getString(ShopPropTable.COLUMN_CODE);
                   Long left = rs.getLong(ShopPropTable.COLUMN_LEFT_AMOUNT);
                   map.put(code, left);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return map;
    }

    public static Long getLeftAmount(String code) {
        String sql = String.format("select %s from %s where %s = '%s' and %s = %d;",
                ShopPropTable.COLUMN_LEFT_AMOUNT,
                ShopPropTable.TABLE_SHOP_PROPS, ShopPropTable.COLUMN_CODE, code,
                ShopPropTable.COLUMN_AVAILABLE , 1);
        LOG.info("getLeftAmount ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                return rs.getLong(ShopPropTable.COLUMN_LEFT_AMOUNT);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0l;
    }

    public static boolean reduceShoPropsLeftAmount(String code, int number) {
        String sql = String.format("update %s set %s = %s - %d where %s = '%s' and %s - %d >= 0 and %s = %d;",
                ShopPropTable.TABLE_SHOP_PROPS,  ShopPropTable.COLUMN_LEFT_AMOUNT, ShopPropTable.COLUMN_LEFT_AMOUNT,
                number, ShopPropTable.COLUMN_CODE, code, ShopPropTable.COLUMN_LEFT_AMOUNT,
                number, ShopPropTable.COLUMN_AVAILABLE , 1);
        LOG.info("reduceShoPropsLeftAmount ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            return num > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
