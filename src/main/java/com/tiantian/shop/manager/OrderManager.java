package com.tiantian.shop.manager;

import com.tiantian.shop.data.postgresql.PGDatabase;
import com.tiantian.shop.data.postgresql.design.OrderTable;
import com.tiantian.shop.entity.OrderEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class OrderManager {
    static Logger LOG = LoggerFactory.getLogger(OrderManager.class);

    public static OrderEntity addOrder(String userId, String shopPropCode, String shopPropName, String addressId, String type) {
        OrderEntity entity = null;
        long current = System.currentTimeMillis();
        String orderId = UUID.randomUUID().toString().replace("-", "");
        //建表时明确，默认为不激活
        String sql = String.format("INSERT INTO %s(%s, %s, %s, %s, %s, %s, %s, %s, %s)" +
                        " VALUES('%s', '%s', '%s', '%s', '%s', %d,  %d, '%s','%s');",
                OrderTable.TABLE_ORDER,
                OrderTable.COLUMN_ORDER_ID, OrderTable.COLUMN_USER_ID, OrderTable.COLUMN_SHOP_PROP_CODE,
                OrderTable.COLUMN_SHOP_PROP_NAME,OrderTable.COLUMN_STATUS,OrderTable.COLUMN_UPDATE_DATE,
                OrderTable.COLUMN_CREATE_DATE,OrderTable.COLUMN_ADDRESS_ID,OrderTable.COLUMN_TYPE,
                orderId, userId, shopPropCode, shopPropName,  "1", current, current, addressId, type);

        LOG.info("addOrder ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            if (num == 1) {
                entity = new OrderEntity(orderId, userId, shopPropCode, shopPropName, "1", addressId, current, current, type);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return entity;
    }

    public static List<OrderEntity> getOrderListPage(String userId, long begin, long count) {
        List<OrderEntity> entityList = new ArrayList<>();
        //建表时明确，默认为不激活
        String sql = String.format("select * from %s where %s = '%s' order by create_date desc limit %d offset %d;",
                OrderTable.TABLE_ORDER, OrderTable.COLUMN_USER_ID, userId, count, begin);

        LOG.info("getOrderListPage ======> sql is {}",sql);
        PreparedStatement preparedStatement=null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                   OrderEntity orderEntity = orderEntityFromSqlRS(rs);
                   entityList.add(orderEntity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return entityList;
    }

    private static OrderEntity orderEntityFromSqlRS(final ResultSet rs) throws SQLException {
        OrderEntity orderEntity = new  OrderEntity();
        orderEntity.setOrderId(rs.getString(OrderTable.COLUMN_ORDER_ID));
        orderEntity.setUserId(rs.getString(OrderTable.COLUMN_USER_ID));
        orderEntity.setShopPropCode(rs.getString(OrderTable.COLUMN_SHOP_PROP_CODE));
        orderEntity.setShopPropName(rs.getString(OrderTable.COLUMN_SHOP_PROP_NAME));
        orderEntity.setStatus(rs.getString(OrderTable.COLUMN_STATUS));
        orderEntity.setCreateDate(rs.getLong(OrderTable.COLUMN_CREATE_DATE));
        orderEntity.setUpdateDate(rs.getLong(OrderTable.COLUMN_UPDATE_DATE));
        orderEntity.setAddressId(rs.getString(OrderTable.COLUMN_ADDRESS_ID));
        orderEntity.setType(rs.getString(OrderTable.COLUMN_TYPE));
        return orderEntity;
    }
}
