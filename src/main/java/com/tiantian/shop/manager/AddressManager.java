package com.tiantian.shop.manager;

import com.tiantian.shop.data.postgresql.PGDatabase;
import com.tiantian.shop.data.postgresql.design.AddressTable;
import com.tiantian.shop.data.postgresql.design.ShopPropTable;
import com.tiantian.shop.entity.AddressEntity;
import com.tiantian.shop.thrift.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 *
 */
public class AddressManager {
    static Logger LOG = LoggerFactory.getLogger(AddressManager.class);

    public static Address addAddress(String userId,  String receiptAddress, String email, String zipCode, String receiverName,
                                           String mobileNumber, String telephoneNumber) {
        Address address = null;
        long current = System.currentTimeMillis();
        String id = UUID.randomUUID().toString().replace("-", "");
        //建表时明确，默认为不激活
        String sql = String.format("INSERT INTO %s(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" +
                        " VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d,  %d);",
                AddressTable.TABLE_ADDRESS,
                AddressTable.COLUMN_ADDRESS_ID, AddressTable.COLUMN_USER_ID,
                AddressTable.COLUMN_RECEIPT_ADDRESS, AddressTable.COLUMN_EMAIL, AddressTable.COLUMN_ZIPCODE,
                AddressTable.COLUMN_RECEIVERNAME,AddressTable.COLUMN_MOBILENUMBER,AddressTable.COLUMN_TELEPHONENUMBER,
                AddressTable.COLUMN_CREATEDATE,AddressTable.COLUMN_UPDATEDATE,
                id, userId, receiptAddress, email, zipCode, receiverName, mobileNumber,
                telephoneNumber, current, current);
        LOG.info("addAddress ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            if (num == 1) {
                address = new Address(id, userId, receiptAddress, email, zipCode,
                        receiverName, mobileNumber, telephoneNumber, current);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return address;
    }

    public static Address getUserAddress(String userId) {
        String sql = String.format("select * from %s where %s = '%s' limit 1;",
        AddressTable.TABLE_ADDRESS, AddressTable.COLUMN_USER_ID, userId);
        LOG.info("addAddress ======> sql is {}",sql);
        Address address = new Address();
        PreparedStatement preparedStatement=null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                   AddressEntity addressEntity = addressEntityFromSqlRS(rs);
                   return new Address(addressEntity.getId(), addressEntity.getUserId(),
                            addressEntity.getReceiptAddress(), addressEntity.getEmail(),
                           addressEntity.getZipCode(),
                           addressEntity.getReceiverName(), addressEntity.getMobileNumber(),
                           addressEntity.getTelephoneNumber(), addressEntity.getCreateDate());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return address;
    }

    public static boolean updateUserAddress(String addressId, String userId, String receiptAddress, String email, String receiverName,
                                            String mobileNumber) {
        Address address = getUserAddress(userId);
        if (address == null || !address.getUserId().equals(userId)) {
            return false;
        }
        String sql = String.format("update %s set %s = '%s',%s = '%s',%s = '%s', %s = '%s', %s = %d where %s = '%s';",
                AddressTable.TABLE_ADDRESS,  AddressTable.COLUMN_RECEIPT_ADDRESS, receiptAddress, AddressTable.COLUMN_EMAIL,
                email, AddressTable.COLUMN_RECEIVERNAME, receiverName, AddressTable.COLUMN_MOBILENUMBER, mobileNumber,
                AddressTable.COLUMN_UPDATEDATE, System.currentTimeMillis(), AddressTable.COLUMN_ADDRESS_ID, addressId);
        LOG.info("updateUserAddress ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            return num > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }



    private static AddressEntity addressEntityFromSqlRS(final ResultSet rs) throws SQLException {
        AddressEntity entity = new  AddressEntity();
        entity.setId(rs.getString(AddressTable.COLUMN_ADDRESS_ID));
        entity.setUserId(rs.getString(AddressTable.COLUMN_USER_ID));
        entity.setReceiptAddress(rs.getString(AddressTable.COLUMN_RECEIPT_ADDRESS));
        entity.setEmail(rs.getString(AddressTable.COLUMN_EMAIL));
        entity.setZipCode(rs.getString(AddressTable.COLUMN_ZIPCODE));
        entity.setReceiverName(rs.getString(AddressTable.COLUMN_RECEIVERNAME));
        entity.setMobileNumber(rs.getString(AddressTable.COLUMN_MOBILENUMBER));
        entity.setTelephoneNumber(rs.getString(AddressTable.COLUMN_TELEPHONENUMBER));
        entity.setCreateDate(rs.getLong(AddressTable.COLUMN_CREATEDATE));
        entity.setUpdateDate(rs.getLong(AddressTable.COLUMN_UPDATEDATE));
        return entity;
    }
}
