package com.tiantian.shop.manager;

import com.tiantian.shop.data.postgresql.PGDatabase;
import com.tiantian.shop.data.postgresql.design.OrderAddressTable;
import com.tiantian.shop.entity.OrderAddressEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

/**
 *
 */
public class OrderAddressManager {
    static Logger LOG = LoggerFactory.getLogger(OrderAddressManager.class);

    public static OrderAddressEntity addOrderAddress(String receiptAddress, String email, String zipCode, String receiverName,
                                     String mobileNumber, String telephoneNumber) {
        OrderAddressEntity address = null;
        long current = System.currentTimeMillis();
        String id = UUID.randomUUID().toString().replace("-", "");
        //建表时明确，默认为不激活
        String sql = String.format("INSERT INTO %s(%s, %s, %s, %s, %s, %s, %s)" +
                        " VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s');",
                OrderAddressTable.TABLE_ORDER_ADDRESS,
                OrderAddressTable.COLUMN_ADDRESS_ID,
                OrderAddressTable.COLUMN_RECEIPT_ADDRESS, OrderAddressTable.COLUMN_EMAIL, OrderAddressTable.COLUMN_ZIPCODE,
                OrderAddressTable.COLUMN_RECEIVERNAME,OrderAddressTable.COLUMN_MOBILENUMBER,OrderAddressTable.COLUMN_TELEPHONENUMBER,
                id, receiptAddress, email, zipCode, receiverName, mobileNumber,
                telephoneNumber);
        LOG.info("addOrderAddress ======> sql is {}",sql);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = PGDatabase.getInstance().getPreparedStatement(sql);
            int num = preparedStatement.executeUpdate();
            if (num == 1) {
                address = new OrderAddressEntity(id, receiptAddress, email, zipCode,
                        receiverName, mobileNumber, telephoneNumber);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (null != preparedStatement){
                try {
                    Connection conn = preparedStatement.getConnection();
                    conn.close();
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return address;
    }
}
