package com.tiantian.shop.data.postgresql.design;

/**
 *
 */
public class AddressTable {
    public static final String TABLE_ADDRESS = "public.address";
    public static final String COLUMN_ADDRESS_ID = "id";
    public static final String COLUMN_USER_ID= "user_id";
    public static final String COLUMN_RECEIPT_ADDRESS = "receipt_address";
    public static final String COLUMN_ZIPCODE = "zip_code";
    public static final String COLUMN_RECEIVERNAME = "receiver_name";
    public static final String COLUMN_MOBILENUMBER = "mobile_number";
    public static final String COLUMN_TELEPHONENUMBER = "telephone_number";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_CREATEDATE = "create_date";
    public static final String COLUMN_UPDATEDATE = "update_date";
}
