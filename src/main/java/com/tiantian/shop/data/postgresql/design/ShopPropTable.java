package com.tiantian.shop.data.postgresql.design;

/**
 *
 */
public class ShopPropTable {
    public static final String TABLE_SHOP_PROPS = "public.shop_props";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_IMG_URL = "img_url";
    public static final String COLUMN_COST_MONEY = "cost_money";
    public static final String COLUMN_COST_SHELL = "cost_shell";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_AVAILABLE = "available";
    public static final String COLUMN_MARK = "mark";
    public static final String COLUMN_CONDITION_DESC = "condition_desc";
    public static final String COLUMN_LEFT_AMOUNT = "left_amount";
}
