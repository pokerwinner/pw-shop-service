package com.tiantian.shop.utils;

import com.alibaba.fastjson.JSON;
import com.tiantian.shop.data.redis.RedisUtil;
import com.tiantian.shop.model.Goods;
import com.tiantian.shop.model.Props;
import com.tiantian.shop.model.ShopProps;
import com.tiantian.shop.model.Wines;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CacheUtils {
    private static final String GOODS_KEY = "goods:";
    private static final String WINES_KEY = "wines:";
    private static final String PROP_KEY = "prop:";
    private static final String SHOP_PROP_KEY = "shop_prop:";

    public static List<Wines> getAllWines(){
       String value = RedisUtil.get(WINES_KEY);
       if (value == null) {
           return new ArrayList<>();
       }
       List winesList = JSON.parseArray(value, Wines.class);
       if (winesList == null) {
           winesList = new ArrayList<>();
       }
       return winesList;
    }

    public static List<Props> getAllProps(){
        String value = RedisUtil.get(PROP_KEY);
        if (value == null) {
            return new ArrayList<>();
        }
        List propsList = JSON.parseArray(value, Props.class);
        if (propsList == null) {
            propsList = new ArrayList<>();
        }
        return propsList;
    }

    public static List<Goods> getAllGoods(){
        String value = RedisUtil.get(GOODS_KEY);
        if (value == null) {
            return new ArrayList<>();
        }
        List goodsList = JSON.parseArray(value, Goods.class);
        if (goodsList == null) {
            goodsList = new ArrayList<>();
        }
        return goodsList;
    }


    public static List<ShopProps> getAllShopProps(){
        String value = RedisUtil.get(SHOP_PROP_KEY);
        if (value == null) {
            return new ArrayList<>();
        }
        List propsList = JSON.parseArray(value, ShopProps.class);
        if (propsList == null) {
            propsList = new ArrayList<>();
        }
        return propsList;
    }

}
