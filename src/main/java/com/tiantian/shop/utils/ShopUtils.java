package com.tiantian.shop.utils;

import com.tiantian.shop.client.ShopClient;
import com.tiantian.shop.client.ShopFactory;
import com.tiantian.shop.settings.ShopConfig;
import com.tiantian.shop.thrift.*;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.util.List;

/**
 *
 */
public class ShopUtils {
    private ObjectPool<ShopClient> pool;
    private ShopUtils(){
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setTestOnBorrow(ShopConfig.getInstance().isTestOnBorrow());
        config.setTestOnCreate(ShopConfig.getInstance().isTestOnCreate());
        pool = new GenericObjectPool<ShopClient>(new ShopFactory(), config);
    }
    private static class GroupUtilsHolder{
        private final static ShopUtils instance = new ShopUtils();
    }
    public static ShopUtils getInstance(){
        return GroupUtilsHolder.instance;
    }
    public String getServiceVersion(){
        ShopClient client = null;
        String version = "";
        try {
            client = pool.borrowObject();
            version=client.getClient().getServiceVersion();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return version;
    }

    public PropsResponse buyProps(String userId, String code){
        ShopClient client = null;
        PropsResponse response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().buyProps(userId, code);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public WinesResponse buyWines(String userId, String code){
        ShopClient client = null;
        WinesResponse response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().buyWines(userId, code);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public List<SpProps> shopList(String userId, long beginIndex, long count){
        ShopClient client = null;
        List<SpProps> response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().shopList(userId, beginIndex, count);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public ShopResponse exchange(String code, String userId){
        ShopClient client = null;
        ShopResponse response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().exchange(code, userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public List<Order> getOrderPage(String userId, long beginIndex, long count) {
        ShopClient client = null;
        List<Order> response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().getOrderPage(userId, beginIndex, count);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public Address getUserAddress(String userId) {
        ShopClient client = null;
        Address response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().getUserAddress(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public Address addUserAddress(String userId,  String receiptAddress, String email, String zipCode, String receiverName,
                                  String mobileNumber, String telephoneNumber)  {
        ShopClient client = null;
        Address response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().addUserAddress(userId, receiptAddress, email, zipCode, receiverName, mobileNumber,
                    telephoneNumber);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public boolean updateUserAddress(String addressId, String userId, String receiptAddress, String email, String zipCode, String receiverName,
                                     String mobileNumber, String telephoneNumber) {
        ShopClient client = null;
        boolean response = false;
        try {
            client = pool.borrowObject();
            response = client.getClient().updateUserAddress(addressId, userId, receiptAddress, email, zipCode, receiverName, mobileNumber, telephoneNumber);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public ShopResponse exchangeJdEcard(String userId, String code) {
        ShopClient client = null;
        ShopResponse response = null;
        try {
            client = pool.borrowObject();
            response = client.getClient().exchangeJdEcard(userId, code);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if(null!= client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }
}
