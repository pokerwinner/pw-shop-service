package com.tiantian.shop.server;

import com.tiantian.shop.data.postgresql.PGDatabase;
import com.tiantian.shop.handler.ShopHandler;
import com.tiantian.shop.settings.PGConfig;
import com.tiantian.shop.settings.ShopConfig;
import com.tiantian.shop.thrift.ShopService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class ShopServer {
    private Logger LOG = LoggerFactory.getLogger(ShopServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {
        ShopServer shopServer = new ShopServer();
        shopServer.init(args);
        shopServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );

            ShopHandler handler = new ShopHandler();
            TProcessor tProcessor = new ShopService.Processor<ShopService.Iface>(handler);
            TServerSocket tss = new TServerSocket(ShopConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", ShopConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
