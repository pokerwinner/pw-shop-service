package com.tiantian.shop.model;

/**
 *
 */
public class Wines {
    private String code;
    private String name;
    private String imgUrl;
    private String desc;
    // 消耗钻石数
    private long costDiamond;
    // 赠与金币数
    private long giftMoney;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getCostDiamond() {
        return costDiamond;
    }

    public void setCostDiamond(long costDiamond) {
        this.costDiamond = costDiamond;
    }

    public long getGiftMoney() {
        return giftMoney;
    }

    public void setGiftMoney(long giftMoney) {
        this.giftMoney = giftMoney;
    }
}
