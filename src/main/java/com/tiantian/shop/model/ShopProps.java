package com.tiantian.shop.model;

/**
 * 商店道具
 */
public class ShopProps {
    private String code;
    private String name;
    private String imgUrl;
    private long costMoney;
    private long costShell;
    private String type; //奖品类型：virtual 虚拟, physical 实物
    private String mark;
    private String conditionDesc;
    private long leftAmount;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getCostMoney() {
        return costMoney;
    }

    public void setCostMoney(long costMoney) {
        this.costMoney = costMoney;
    }

    public long getCostShell() {
        return costShell;
    }

    public void setCostShell(long costShell) {
        this.costShell = costShell;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public long getLeftAmount() {
        return leftAmount;
    }

    public void setLeftAmount(long leftAmount) {
        this.leftAmount = leftAmount;
    }

    public String getConditionDesc() {
        return conditionDesc;
    }

    public void setConditionDesc(String conditionDesc) {
        this.conditionDesc = conditionDesc;
    }
}
