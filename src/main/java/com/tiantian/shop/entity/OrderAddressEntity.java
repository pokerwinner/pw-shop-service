package com.tiantian.shop.entity;

/**
 *
 */
public class OrderAddressEntity {
    private String id;
    private String receiptAddress;
    private String zipCode;
    private String receiverName;
    private String mobileNumber;
    private String telephoneNumber;
    private String email;

    public OrderAddressEntity(String id, String receiptAddress, String zipCode, String receiverName,
                              String mobileNumber, String telephoneNumber, String email) {
        this.id = id;
        this.receiptAddress = receiptAddress;
        this.zipCode = zipCode;
        this.receiverName = receiverName;
        this.mobileNumber = mobileNumber;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiptAddress() {
        return receiptAddress;
    }

    public void setReceiptAddress(String receiptAddress) {
        this.receiptAddress = receiptAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
