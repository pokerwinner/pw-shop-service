package com.tiantian.shop.client;

import com.tiantian.shop.settings.ShopConfig;
import com.tiantian.shop.thrift.ShopService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 *
 */
public class ShopClient {
    private TTransport transport;
    private ShopService.Client client;

    public ShopService.Client getClient() {
        return client;
    }

    public void setClient(ShopService.Client client) {
        this.client = client;
    }

    public ShopClient() {
        try {
            transport = new TSocket(ShopConfig.getInstance().getHost(),
                    ShopConfig.getInstance().getPort());
            transport.open();
            TFastFramedTransport fastTransport = new TFastFramedTransport(transport);

            TCompactProtocol protocol = new TCompactProtocol(fastTransport);

            client = new ShopService.Client(protocol);
        } catch (TException x) {
            x.printStackTrace();
        }
    }

    public void close() {
        if (null != transport) {
            transport.close();
            client = null;
        }
    }
}
