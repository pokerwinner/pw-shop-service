namespace java com.tiantian.shop.thrift

const string version = '1.0.0'

struct ShopResponse {
  1:i32 status,
  2:string message
}


struct WinesResponse {
  1:i32 status,
  2:string message,
  3:i64 giftMoney,
}
struct PropsResponse {
  1:i32 status,
  2:string message
}

struct SpProps {
    1:string code
    2:string name
    3:string imgUrl
    4:i64 costMoney
    5:i64 costShell
    6:string type
    7:string mark
    8:string conditionDesc
    9:i64 leftAmount
}

struct Order {
     1:string orderId
     2:string userId
     3:string shopPropCode
     4:string shopPropName
     5:string status
     6:i64 createDate
}

struct Address {
    1:string id
    2:string userId
    3:string receiptAddress
    4:string email
    5:string zipCode
    6:string receiverName
    7:string mobileNumber
    8:string telephoneNumber
    9:i64 createDate
}

service ShopService{
    string getServiceVersion(),
    WinesResponse buyWines(1:string userId, 2:string code),
    PropsResponse buyProps(1:string userId, 2:string code),
    ShopResponse exchange(1:string code, 2:string userId)
    list<SpProps> shopList(1:string userId, 2:i64 beginIndex, 3:i64 count)
    list<Order> getOrderPage(1:string userId, 2:i64 beginIndex, 3:i64 count)
     Address getUserAddress(1:string userId),
     Address addUserAddress(1:string userId,  2:string receiptAddress, 3:string email,
                            4:string zipCode, 5:string receiverName,
                                       6:string mobileNumber, 7:string telephoneNumber),

     bool updateUserAddress(1:string addressId, 2:string userId, 3:string receiptAddress,
                    4:string email, 5:string zipCode, 6:string receiverName, 7:string mobileNumber, 8:string telephoneNumber),
      ShopResponse exchangeJdEcard(1:string userId, 2:string code)
}